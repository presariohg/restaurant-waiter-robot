from gpiozero           import Button

from pcd8544.lcd        import  LCD, ON
from font10x10          import  font10x10
import os

SERVER_ADDRESS      =   "192.168.15.100"

def button_init():

    BUTTON_1        =   Button(4)
    BUTTON_2        =   Button(17)
    BUTTON_3        =   Button(5)
    BUTTON_4        =   Button(6)

    global buttons

    buttons         =   [BUTTON_1, BUTTON_2, BUTTON_3, BUTTON_4]

def lcd_init():
    SCLK = 14 
    DIN  = 12 
    DC   = 4 
    CS   = 10 
    RST  = 5 
    BL   = 30

    PIN_OUT     =   {  
                    'SCLK'  :   SCLK,
                    'DIN'   :   DIN,
                    'DC'    :   DC,
                    'CS'    :   CS,
                    'RST'   :   RST,
                    'LED'   :   BL, #backlight   
    }
    global lcd_1

    lcd_1   =   LCD(PIN_OUT)
    lcd_1.set_backlight(ON)
    lcd_1.set_font(font10x10)

def wait_button(button):
    while(button.is_pressed):
        pass

button_init()
lcd_init()

lcd_1.clear()
lcd_1.put_string('0')
lcd_1.refresh()

while (1):
    if (buttons[0].is_pressed):
        wait_button(buttons[0])

        lcd_1.clear()
        lcd_1.put_string('1')
        lcd_1.refresh()

        os.system("mosquitto_pub -t /server/waiter/ -m 'T1#' -h {}".format(SERVER_ADDRESS), )

    if (buttons[1].is_pressed):
        wait_button(buttons[1])

        lcd_1.clear()
        lcd_1.put_string('2')
        lcd_1.refresh()

        os.system("mosquitto_pub -t /server/waiter/ -m 'T2#' -h {}".format(SERVER_ADDRESS), )

    if (buttons[2].is_pressed):
        wait_button(buttons[2])

        lcd_1.clear()
        lcd_1.put_string('3')
        lcd_1.refresh()

        os.system("mosquitto_pub -t /server/waiter/ -m 'T3#' -h {}".format(SERVER_ADDRESS), )

    if (buttons[3].is_pressed):
        wait_button(buttons[3])

        lcd_1.clear()
        lcd_1.put_string('4')
        lcd_1.refresh()

        os.system("mosquitto_pub -t /server/waiter/ -m 'T4#' -h {}".format(SERVER_ADDRESS), )